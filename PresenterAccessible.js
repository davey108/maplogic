let dbOps = require('./DatabaseOps');

/**
 * Wrapper for the presenter to access to display to view
 * @param userLocation
 * @param display
 */
let showNearestBathrooms = (userLocation, display = 20) => {
    return dbOps.findClosestBathroom(userLocation, display).then(res => {
        res = res.map(e => {
            if(!e.AccumulateRating|| !e.NumRatings) e['BathroomRating'] = 0;
            else e['BathroomRating'] = e.AccumulateRating/e.NumRatings;
            delete e.AccumulateRating;
            delete e.NumRatings;
            return e;
        }).splice(0, display);
        let jsonObj = {};
        res.forEach((e, i) => {
            jsonObj[i] = e;
        });
        return res;
    }).catch(()=> {
       return {status: 400, message: 'Error accessing internal services'};
    });
};

let showNearestResultBathrooms = async (name) => {
  return dbOps.findMatchingBathrooms(name).then(result => {
      let res = result.map(e => {
          if(!e.AccumulateRating|| !e.NumRatings) e['BathroomRating'] = 0;
          else e['BathroomRating'] = e.AccumulateRating/e.NumRatings;
          delete e.AccumulateRating;
          delete e.NumRatings;
          return e;
      });
      let jsonObj = {};
      res.forEach((e, i) => {
          jsonObj[i] = e;
      });
      return res;
  }).catch(() => {
     return {status: 400, message: 'Error accessing internal services'};
  });
};

let userRateBathroom = (name, givenRating) => {
  return dbOps.saveRating(name, givenRating).then(result => {
      return result;
  });
};

let userInputBathroom = (name, latitude, longitude) => {
  return dbOps.saveBathroom(name, {latitude: latitude, longitude: longitude}).then(result => {
      console.log(result);
      return result;
  });
};

module.exports = {
    showNearestBathrooms,
    userRateBathroom,
    showNearestResultBathrooms,
    userInputBathroom
};