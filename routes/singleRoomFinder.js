let express = require('express');
let router = express.Router();
let presenterAccessible = require('../PresenterAccessible');


router.get('/', (req, res) => {
  res.send("You reached this page!");
});
/* Put in the new bathroom to the system */
router.post('/', function(req, res){
  let name = req.body.roomName;
  if(!req.body.hasOwnProperty('latitude') || !req.body.hasOwnProperty('longitude')){
    res.send({status: 500, message: "Missing the location of the bathroom"});
  }
  else{
   if(presenterAccessible.userInputBathroom(name, req.body.latitude, req.body.longitude))
    res.send({status: 200, message: "Successfully added the bathroom: " + name + " into the system"});
   else{
     res.send({status: 400, message: "Internal problem with the system"});
   }
  }
});

/* Put in the rating of the users of the bathroom */
router.put('/rating', function(req, res) {
  let name = req.body.roomName;
  if(!req.body.rating) res.send({status: 500, message: "Missing the rating of the room"});
  else {
    if(presenterAccessible.userRateBathroom(name, req.body.rating)) res.send({status: 200, message: "Successfully stored the rating of the bathroom"});
    else{
      res.send({status: 400, message: "Internal problem with the system"});
    }
  }
});



module.exports = router;
