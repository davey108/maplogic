let express = require('express');
let router = express.Router();
let presenterAccessible = require('../PresenterAccessible');

/* GET the closest number of restrooms*/
router.get('/user', function(req, res, next) {
  if(req.query.hasOwnProperty('name')) next();
  else{
      console.log(req.query);
    if(req.query.hasOwnProperty('latitude') && req.query.hasOwnProperty('longitude')){
      let display = 20;
      if(req.query.hasOwnProperty('prefDisplay')) display = req.query.prefDisplay;
      presenterAccessible.showNearestBathrooms({latitude: req.query.latitude, longitude: req.query.longitude}, display)
          .then(result => {
            res.send(result);
          }).catch(err => {
            res.send(err);
      });
    }
    else{
      res.send({status: 500, message: "Missing the user location"});
    }
  }
});

/* GET the closest based on the search string */
router.get('/user', (req, res) => {
  presenterAccessible.showNearestResultBathrooms(req.query.name).then(result => {
    res.send(result);
  }).catch(err => {
    res.send(err);
  });
});

module.exports = router;
