let mysql = require('mysql2');
const geolib = require('geolib');
const cred = require('./credentials');
const {filter} = require('fuzzaldrin');

const conn = mysql.createConnection({
    host: 'sql9.freesqldatabase.com',
    user: cred.dbUser,
    password: cred.password,
    database: cred.database,
    port: 3306
});

/**
 * Find the closest bathroom to the user
 * @param userLocation a user location object containing the user's lat and long
 * @param maxDisplay max number of bathroom to return
 * @return a json object containing all the closest bathroom
 * @throw an error displaying what's the problem
 */
let findClosestBathroom = async (userLocation, maxDisplay = 20) =>{
    if(!userLocation.hasOwnProperty('latitude') || !userLocation.hasOwnProperty('longitude')){
        throw 'User Location Object missing either Latitude or Longitude';
    }
    try{
        let res = await promisifyQuery('select * from Bathrooms');
        console.log(res);
        let closestLocations = res.sort((a, b) => {
            return geolib.getDistance({latitude: a.Latitude, longitude: a.Longitude}, {latitude: userLocation.latitude, longitude: userLocation.longitude}) -
                geolib.getDistance({latitude: b.Latitude, longitude: b.Longitude}, {latitude: userLocation.latitude, longitude: userLocation.longitude})
        });
        return closestLocations;
    }
    catch(err){
        console.log(err);
        throw err;
    }
};

/**
 * Find a certain bathroom information, given the name
 * @param name a string indicating the bathroom to search for
 * @returns a promise containing the information about the matching places
 */
let findMatchingBathrooms = async (name) => {
    try{
        let res = await promisifyQuery('select * from Bathrooms');
        return filter(res, name, {key: 'BathroomName', maxResults: 5});
    }
    catch(err){
        throw err;
    }
};
/**
 * Save the rating and re-average them
 * @param name the name of the bathroom
 * @param rating the newly added rating score (1-5)
 * @returns a promise containing the information about the result
 */
let saveRating = async (name, rating) => {
    try{
        let res = await promisifyQuery('select * from Bathrooms where `BathroomName` = ?', [name]);
        let bathroom = res[0];
        let currNumRating = bathroom.NumRatings + 1;
        let totalScore = bathroom.AccumulateRating + rating;
        await promisifyQuery('update Bathrooms set AccumulateRating = ?, NumRatings = ? where BathroomName = ?',
            [totalScore, currNumRating, name]);
        return true;
    }
    catch(err){
        return false;
    }
};

/**
 * Save a brand new bathroom to db
 * @param name the name of the bathroom
 * @param location the location of the bathroom including latitude and longitude
 * @return the result of the saving
 */
let saveBathroom = async(name, location) => {
  if(!location.hasOwnProperty('latitude') || !location.hasOwnProperty('longitude')){
      throw 'User Location Object missing either Latitude or Longitude';
  }
  try{
      let checking = await promisifyQuery('select * from Bathrooms where `BathroomName` = ?', [name]);
      if(checking.length) return false;
      console.log("Ok");
      await promisifyQuery('insert into Bathrooms (BathroomName, Latitude, Longitude) values (?, ?, ?)', [name, location.latitude, location.longitude]);
      return true;
  }
  catch(err){
      console.log(err);
      return false
  }
};
let promisifyQuery = (query, params = []) => {
   return new Promise((resolve, reject) => {
       conn.execute(query, params, (err, result) => {
           if(err) reject(err);
           else resolve(result);
       });
   });
};

module.exports = {
    findClosestBathroom,
    findMatchingBathrooms,
    saveRating,
    saveBathroom
};




